/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #3: Smart Dam
   Author:
   - Andrea Casadei 800898
*/

// #include "SmartDumpsterGlobal.h"
#include "BluetoothMsgService.h"
#include "Arduino.h"

String content;

BluetoothMsgService bluetooth;

void
BluetoothMsgService::init(String deviceName, int rxPin, int txPin) {
	unsigned long baudRate = 9600;
    #ifndef DEBUG
        channel = new SoftwareSerial(rxPin, txPin);
	    channel->begin(baudRate);
        channel->print("AT+NAME" + deviceName);
        delay(1000);
        while(channel->read() >= 0);
    #endif
	content.reserve(64);
	content = "";
    msgAvailable = false;
    currentMsg = NULL;
}

bool
BluetoothMsgService::isMsgAvailable() {
    #ifdef DEBUG
        while(Serial.available()) {
            char ch = (char) Serial.read();
    #else
        while(this->channel->available()) {
            char ch = (char) channel->read();
    #endif
        // if(ch == SmartDumpsterGlobal::endChar) {
        //     currentMsg = new Msg(content);
        //     content = "";
        //     msgAvailable = true;
        //     return true;
        // } else {
        //     content += ch;
        // }
    }
    return false;
}

Msg*
BluetoothMsgService::receiveMsg() {
    if (msgAvailable) {
        Msg* msg = currentMsg;
        Serial.println(msg->getContent());
        msgAvailable = false;
        currentMsg = NULL;
        content = "";
        return msg;
    } else {
        return NULL;
    }
}

void
BluetoothMsgService::sendMsg(const String& msg) {
    #ifdef DEBUG
        Serial.println(msg);
    #else
	    channel->print(msg + '\n');
    #endif
}
