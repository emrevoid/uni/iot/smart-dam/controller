/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #3: Smart Dam
   Author:
   - Andrea Casadei 800898
*/

// #include "ClosedTask.h"
// #include "ClosingTask.h"
// #include "OpenedTask.h"
// #include "OpeningTask.h"
// #include "TrashType.h"
// #include "SmartDumpsterGlobal.h"
// #include "Scheduler.h"
#include "StateType.h"
#include "BluetoothMsgService.h"
#include "JsonMsgService.h"
#include "ServoMotorImpl.h"
#include "LedImpl.h"

#include <Arduino.h>

#define PIN_SERVO 10
#define PIN_BT_RX 5
#define PIN_BT_TX 6
#define JSON_MAX_ELEMS 10
#define PIN_LED 4

// Scheduler scheduler;
StateType STATE;
JsonMsgService *jsonMsgService;
ServoMotor* motor;
Led* led;

void setup() {
    // Serial.begin(9600);
    //   scheduler.init(50);

    // bluetooth.init(SmartDumpsterGlobal::deviceName, PIN_BT_RX, PIN_BT_TX);
    // jsonMsgService = new JsonMsgService(JSON_MAX_ELEMS);
    // pinMode(PIN_SERVO, OUTPUT);
    // motor = new ServoMotorImpl(PIN_SERVO);
    // pinMode(PIN_LED_1, OUTPUT);
    // leds[TrashType::GLASS] = new LedImpl(PIN_LED_1);
    // pinMode(PIN_LED_2, OUTPUT);
    // leds[TrashType::PAPER] = new LedImpl(PIN_LED_2);
    // pinMode(PIN_LED_3, OUTPUT);
    // leds[TrashType::PLASTIC] = new LedImpl(PIN_LED_3);

    // ClosedTask* closedTask = new ClosedTask(jsonMsgService);
    // closedTask->init(100);

    // OpeningTask* openingTask = new OpeningTask(motor, leds, jsonMsgService);
    // openingTask->init(500);

    // OpenedTask* openedTask = new OpenedTask(jsonMsgService);
    // openedTask->init(100);

    // ClosingTask* closingTask = new ClosingTask(motor, leds, jsonMsgService);
    // closingTask->init(500);

    // scheduler.addTask(closedTask);
    // scheduler.addTask(openingTask);
    // scheduler.addTask(openedTask);
    // scheduler.addTask(closingTask);
}

void loop() {
  // scheduler.schedule();


  switch(STATE){
    case 2: break;
    case 3: break;
    default: break;
  }
}
