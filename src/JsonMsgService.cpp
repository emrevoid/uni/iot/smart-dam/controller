/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #3: Smart Dam
   Author:
   - Andrea Casadei 800898
*/

#include "Arduino.h"
#include "JsonMsgService.h"
// #include "SmartDumpsterGlobal.h"

JsonMsgService::JsonMsgService(int jsonMaxElems) {
    capacity = JSON_OBJECT_SIZE(jsonMaxElems);
    // trashType = TrashType::NOTHING;
    // amount = SmartDumpsterGlobal::maxTime;
    request = "";
    event = "";
}

String 
JsonMsgService::getEvent(){
    return event;
}

// TrashType
// JsonMsgService::getTrashType(){
//     return trashType;
// }

String
JsonMsgService::getRequest(){
    return request;
}

unsigned 
JsonMsgService::getAmount(){
    return amount;
}

bool 
JsonMsgService::isJsonAvailable(){
    if (bluetooth.isMsgAvailable()) {
        currentMsg = bluetooth.receiveMsg();
        DynamicJsonDocument json(capacity);
        DeserializationError error = deserializeJson(json, currentMsg->getContent());
        if (!error) {    /*        
            if (json.containsKey(SmartDumpsterGlobal::requestSymbol) &&
                json[SmartDumpsterGlobal::requestSymbol].is<String>()) {
                    request = json[SmartDumpsterGlobal::requestSymbol].as<String>();
            } else {
                    request = "";
            }
            if (json.containsKey(SmartDumpsterGlobal::trashTypeSymbol) &&
                json[SmartDumpsterGlobal::trashTypeSymbol].is<unsigned>() &&
                json[SmartDumpsterGlobal::trashTypeSymbol].as<unsigned>() <= SmartDumpsterGlobal::maxTrash) {
                    trashType = static_cast<TrashType>(json[SmartDumpsterGlobal::trashTypeSymbol].as<unsigned>());
            } else {
                    trashType = TrashType::NOTHING;
            }
            if (json.containsKey(SmartDumpsterGlobal::amountSymbol) &&
                json[SmartDumpsterGlobal::amountSymbol].is<unsigned>()) {
                    amount = json[SmartDumpsterGlobal::amountSymbol].as<unsigned>();
            } else {
                    amount = SmartDumpsterGlobal::maxTime;
            }
            if (json.containsKey(SmartDumpsterGlobal::eventSymbol) &&
                json[SmartDumpsterGlobal::eventSymbol].is<String>()) {
                    event = json[SmartDumpsterGlobal::eventSymbol].as<String>();
            } else {
                    event = "";
            }*/
            return true;
        } else {
            Serial.println(F("JsonMsgService: Error in json deserialization."));
            Serial.println(error.c_str());
        }
    }
    delete currentMsg;
    currentMsg = nullptr;
    return false;
}

void
JsonMsgService::sendEvent(const String& event){
    DynamicJsonDocument json(capacity);
    String msg;
    json["event"] = event;
    serializeJson(json, msg);
    bluetooth.sendMsg(msg);
}

void
JsonMsgService::sendExtend(const unsigned amount){
    DynamicJsonDocument json(capacity);
    String msg;
    json["request"] = "extend_time",
    json["amount"] = amount;
    serializeJson(json, msg);
    bluetooth.sendMsg(msg);
}

void
JsonMsgService::sendEcho(){
    bluetooth.sendMsg(currentMsg->getContent());
};