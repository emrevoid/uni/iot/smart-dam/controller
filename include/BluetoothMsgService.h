/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #3: Smart Dam
   Author:
   - Andrea Casadei 800898
*/

#ifndef BLUETOOTHMSGSERVICE_H
#define BLUETOOTHMSGSERVICE_H

#include "Arduino.h"
#include "SoftwareSerial.h"
#include "ArduinoJson.h"

/**
 * A message to be exchanged between endpoints.
 */
class Msg {
	String content;

  public:
	/**
	 * Instanciates a message.
	 * @param content Message content.
	 */
	Msg(String content) { this->content = content; }

	/**
	 * Get the message content.
	 * @return the message content.
	 */
	String getContent() { return content; }
};

/**
 * A controller for sending and receiving messages.
 * @see Msg
 */
class BluetoothMsgService {
  public:
    Msg* currentMsg;
    bool msgAvailable;

	/**
	 * Initialize the Bluetooth message controller.
     * @param deviceName the device name to be set.
     * @param rxPin receiving pin.
     * @param txPin sending pin.
	 */
	void init(String deviceName, int rxPin, int txPin);

	/**
	 * Check if a message is available.
	 * @return if a message is available.
	 */
	bool isMsgAvailable();

	/**
	 * Receive and save a message.
	 * @see Msg
	 */
	Msg* receiveMsg();

	/**
	 * Send a message.
	 * @param msg the message to be sent.
	 */
	void sendMsg(const String& msg);

  private:
    SoftwareSerial* channel;
};

extern BluetoothMsgService bluetooth;

#endif

