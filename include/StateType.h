/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #3: Smart Dam
   Author:
   - Andrea Casadei 800898
*/

#ifndef STATETYPE_H
#define STATETYPE_H

/**
 * Represents the Smart Dam Sstate.
 */
enum StateType {
	NORMAL,
	PREALARM,
    ALARM,
	MANUAL
};

#endif
